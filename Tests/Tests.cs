using System;
using Xunit;
using EasyUDP.Utilities;

namespace EasyUDP.Tests
{
    public class Tests : IDisposable
    {
        private Client client;
        private Server server;
        private Message receivedMessage;

        public Tests()
        {
            server = new Server();
            client = new Client();

            server.onMessageReceived += (message) => receivedMessage = message;
        }

        public void Dispose()
        {
            client.Close();
            server.Close();
        }

        [Fact]
        public void ReceiveMessageTest()
        {
            var serverIP = Utils.LocalIP.ToString();
            client.Connect(serverIP);

            client.Send("TestMessage");
            server.Listen();

            Assert.Equal("TestMessage", receivedMessage.value);
        }

        [Fact]
        public async void AutoConnectTest()
        {
            var searchOperation = client.AutoConnect();
            server.Listen();

            await searchOperation;

            Assert.Equal("PING", receivedMessage.value);
            Assert.Contains(Utils.LocalIP.ToString(), receivedMessage.sender.ToString());
        }
    }
}